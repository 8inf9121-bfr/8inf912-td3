#include <iostream>

#include "../library/includes/ReplicationManager.h"

int main() {
	uqac::replication::ReplicationManager myReplicationManager;
	int a;
	std::cout << "To start as server, type 0, to start as client, type 1." << std::endl;
	std::cin >> a;
	if (a == 0) {
		myReplicationManager.game(true);
	}
	else {
		myReplicationManager.game(false);
	}
}