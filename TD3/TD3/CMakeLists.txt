﻿# CMakeList.txt : projet CMake pour TD3, incluez la source et définissez
# la logique spécifique au projet ici.
#
cmake_minimum_required (VERSION 3.8)

add_subdirectory ("externals")
add_subdirectory ("library")
add_subdirectory ("myGame")

# Ajoutez une source à l'exécutable de ce projet.
add_executable (TD3 "TD3.cpp" "TD3.h")
target_link_libraries(TD3 PUBLIC Replicate Serialize Network)

# TODO: Ajoutez des tests et installez des cibles si nécessaire.