﻿// TD3.cpp : définit le point d'entrée de l'application.
//

#include "TD3.h"
#include "library/includes/Player.h"
#include "library/includes/Ennemy.h"

using namespace std;
using namespace uqac::replication;

void testingLinkingContext() {
	/***************************
		Testing LinkingContext
	****************************/
	/*cout << "---- Testing LinkingContext ----" << endl;

	LinkingContext myLinkingContext;

	NetworkObject player;
	NetworkObject cat;
	NetworkObject weapon;
	NetworkObject watermelon;
	NetworkObject flask;

	// Testing addObjectToContextWithId //
	int check = myLinkingContext.addObjectToContextWithId(&player, 1);
	if (check == EXIT_SUCCESS) {
		std::cout << "addObjectToContextWithId: add an object is OK" << std::endl;
	}
	else {
		std::cout << "addObjectToContextWithId: add an object is KO" << std::endl;
	}
	check = myLinkingContext.addObjectToContextWithId(&cat, 2);
	if (check == EXIT_SUCCESS) {
		std::cout << "addObjectToContextWithId: add another object is OK" << std::endl;
	}
	else {
		std::cout << "addObjectToContextWithId: add another object is KO" << std::endl;
	}
	check = myLinkingContext.addObjectToContextWithId(&weapon, 2);
	if (check == EXIT_FAILURE) {
		std::cout << "addObjectToContextWithId: prevent adding object with same identifier is OK" << std::endl;
	}
	else {
		std::cout << "addObjectToContextWithId: prevent adding object with same identifier is KO" << std::endl;
	}

	// Testing addObjectToContext
	check = myLinkingContext.addObjectToContext(&watermelon);
	if (check == -2147483648) {
		std::cout << "addObjectToContex: adding object without id is OK" << std::endl;
	}
	else {
		std::cout << "addObjectToContex: adding object without id is KO" << std::endl;
	}

	check = myLinkingContext.addObjectToContext(&watermelon);
	if (check == -2147483647) {
		std::cout << "addObjectToContex: adding another object without id is OK" << std::endl;
	}
	else {
		std::cout << "addObjectToContex: adding another object without id is KO" << std::endl;
	}

	// Testing getIdentifier //
	optional<int> id = myLinkingContext.getIdentifier(&player);
	if (id.has_value()) {
		check = *id;
	}
	else {
		check = -1;
	}
	if (check == 1) {
		std::cout << "getIdentifier: get existing id is OK" << std::endl;
	}
	else {
		std::cout << "getIdentifier: get existing id is KO" << std::endl;
	}

	id = myLinkingContext.getIdentifier(&weapon);
	if (id.has_value()) {
		check = *id;
	}
	else {
		check = -1;
	}
	if (check == -1) {
		std::cout << "getIdentifier: get non existing id is OK" << std::endl;
	}
	else {
		std::cout << "getIdentifier: get non existing id is KO" << std::endl;
	}

	// Testing getObjectPointer //
	NetworkObject* pointer = nullptr;
	optional<NetworkObject*> mayBePointer = myLinkingContext.getObjectPointer(1);
	if (mayBePointer.has_value()) {
		pointer = *mayBePointer;
	}
	else {
		pointer = nullptr;
	}
	if (pointer == &player) {
		std::cout << "getObjectPointer: get existing pointer is OK" << std::endl;
	}
	else {
		std::cout << "getObjectPointer: get existing pointer is KO" << std::endl;
	}

	mayBePointer = myLinkingContext.getObjectPointer(3);
	if (mayBePointer.has_value()) {
		pointer = *mayBePointer;
	}
	else {
		pointer = nullptr;
	}
	if (pointer == nullptr) {
		std::cout << "getObjectPointer: get non existing pointer is OK" << std::endl;
	}
	else {
		std::cout << "getObjectPointer: get non existing pointer is KO" << std::endl;
	}

	// Testing removeObjectInContext //
	myLinkingContext.removeObjectInContext(&player);
	// Check if it has been removed from first map
	mayBePointer = myLinkingContext.getObjectPointer(1);
	if (mayBePointer.has_value()) {
		pointer = *mayBePointer;
	}
	else {
		pointer = nullptr;
	}
	// Test removing from second map
	id = myLinkingContext.getIdentifier(&player);
	if (id.has_value()) {
		check = *id;
	}
	else {
		check = -1;
	}
	if ((pointer == nullptr) && (check == -1)) {
		std::cout << "removeObjectInContext: removing existing pointer is OK" << std::endl;
	}
	else {
		std::cout << "removeObjectInContext: removing existing pointer is KO" << std::endl;
	}

	check = myLinkingContext.removeObjectInContext(&weapon);
	if (check == EXIT_FAILURE) {
		std::cout << "removeObjectInContext: removing non existing pointer is OK" << std::endl;
	}
	else {
		std::cout << "removeObjectInContext: removing non existing pointer is KO" << std::endl;
	}*/
}

void testPlayer() {
	uqac::utils::Vector3<float> position(100.f, 88.f, -88.f);
	uqac::utils::Quaternion rotation(-0.707f, 0.707f, 0.f, 0.f);
	uqac::utils::Vector3<float> size(25.f, -25.f, 0.f);
	int life(88); 
	int armor(5); 
	float money(88888.6123f); 
	std::string name("Faker");
	Player p(position, rotation, size, life, armor, money, name);

	std::cout << "\tTEST PLAYER BEGIN" << std::endl;

	uqac::compression::Compression* myCompressor =  new uqac::compression::Compression();
	p.Write(myCompressor, true);
	uqac::compression::Decompression* myDecompressor = new uqac::compression::Decompression(myCompressor->getSerializerContent().c_str(), myCompressor->getSerializerContent().size());
	p.Read(myDecompressor, true);

	std::cout << "\tTEST PLAYER END" << std::endl;
}

void testPlayerDefault() {
	Player p;

	std::cout << "\tTEST PLAYER DEFAULT BEGIN" << std::endl;

	uqac::compression::Compression* myCompressor = new uqac::compression::Compression();
	p.Write(myCompressor, true);
	uqac::compression::Decompression* myDecompressor = new uqac::compression::Decompression(myCompressor->getSerializerContent().c_str(), myCompressor->getSerializerContent().size());
	p.Read(myDecompressor, true);

	std::cout << "\tTEST PLAYER DEFAULT END" << std::endl;
}

void testEnnemy() {
	uqac::utils::Vector3<float> position(100.f, 88.f, -88.f);
	uqac::utils::Quaternion rotation(-0.707f, 0.707f, 0.f, 0.f);
	EnnemyType type = EnnemyType::boss;
	int life(56);
	Ennemy e(position, rotation, type, life);

	std::cout << "\tTEST ENNEMY BEGIN" << std::endl;

	uqac::compression::Compression* myCompressor = new uqac::compression::Compression();
	e.Write(myCompressor, true);
	uqac::compression::Decompression* myDecompressor = new uqac::compression::Decompression(myCompressor->getSerializerContent().c_str(), myCompressor->getSerializerContent().size());
	e.Read(myDecompressor, true);

	std::cout << "\tTEST ENNEMY END" << std::endl;
}

void testEnnemyDefault() {
	Ennemy e;

	std::cout << "\tTEST ENNEMY DEFAULT BEGIN" << std::endl;

	uqac::compression::Compression* myCompressor = new uqac::compression::Compression();
	e.Write(myCompressor, true);
	uqac::compression::Decompression* myDecompressor = new uqac::compression::Decompression(myCompressor->getSerializerContent().c_str(), myCompressor->getSerializerContent().size());
	e.Read(myDecompressor, true);

	std::cout << "\tTEST ENNEMY DEFAULT END" << std::endl;
}

void testingReplicationManager() {
	ReplicationManager emile;
	std::cout << "Je suis emile" << endl;

	//std::cout << "ClassId:" << ::mClassID << std::endl;

	uqac::utils::Vector3<float> position(100.f, 88.f, -88.f);
	uqac::utils::Quaternion rotation(-0.707f, 0.707f, 0.f, 0.f);
	EnnemyType type = EnnemyType::boss;
	int life(56);
	std::shared_ptr e = std::make_shared<Ennemy>(position, rotation, type, life);

	emile.addObject(e);

	uqac::utils::Vector3<float> size(25.f, -25.f, 0.f);
	int life2(88);
	int armor(5);
	float money(88888.6123f);
	std::string name("Faker");
	std::shared_ptr p = std::make_shared<Player>(position, rotation, size, life2, armor, money, name);
	//Player p(position, rotation, size, life2, armor, money, name);

	emile.addObject(p);

	emile.addObject(e);

	std::shared_ptr p2 = std::make_shared<Player>();
	emile.addObject(p2);

	std::cout << "Serialized Datas: " << std::endl;
	emile.sendUpdate();
}


int main()
{
	//testingLinkingContext();
	testingReplicationManager();

	testPlayer();
	testPlayerDefault();
	testEnnemy();
	testEnnemyDefault();
	return 0;
}
