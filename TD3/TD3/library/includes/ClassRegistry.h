#pragma once
#include <map>
#include <functional>

#include "NetworkObject.h"

namespace uqac::replication {
	class ClassRegistry {
	public:
		inline static ClassRegistry& getInstance() {
			static ClassRegistry instance;  // Guaranteed to be destroyed.
											// Instantiated on first use.
			return instance;
		}

		ClassRegistry(ClassRegistry const&) = delete;
        void operator=(ClassRegistry const&) = delete;

		/*
		save a class in the map
		*/
		template <class T>
		static void save(T object);

		/*
		Uses the class creation method stored int the map to create a new network object
		@param int is the class id, considered always valid
		@return returns a created object
		*/
		static std::shared_ptr<NetworkObject> create(int);

		inline bool classExists(int classId) { return !(myMap.find(classId) == myMap.end()); }

	protected:
		//static ClassRegistry* instance;
	
	private:
		// int is class id
		std::map<int, std::function<std::shared_ptr<NetworkObject>()>> myMap;

		ClassRegistry() = default;
		~ClassRegistry() = default;
	};

	//ClassRegistry* ClassRegistry::instance = new ClassRegistry();

	//inline ClassRegistry* ClassRegistry::getInstance() { return instance; }
}

#include "ClassRegistry.ipp"
