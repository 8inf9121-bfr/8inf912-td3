#pragma once

#include "NetworkObject.h"
#include "Framework.h"
#include "Compression.hpp"

namespace uqac::replication {
	enum EnnemyType { boss, sbire };

	class Ennemy : public NetworkObject {
	private:
		uqac::utils::Vector3<float> position;
		uqac::utils::Quaternion rotation;
		enum EnnemyType type;
		int life;

		std::pair<int, int> mClassIDRange;
		std::pair<int, int> mTypeRange;
		std::pair<float, float> positionXRange;
		std::pair<float, float> positionYRange;
		std::pair<float, float> positionZRange;
		std::pair<float, float> quaternionRange;
		std::pair<int, int> lifeRange;
		int defaultPrecision;

		void initRanges();

	public:
		Ennemy();
		Ennemy(uqac::utils::Vector3<float>, uqac::utils::Quaternion, EnnemyType, int);
		~Ennemy() = default;

		virtual void Write(uqac::compression::Compression* compressor, bool verbose = false) override;
		virtual void Read(uqac::compression::Decompression* decompressor, bool verbose = false) override;
		virtual void Destroy(bool verbose) override;
		virtual void randomModifications() override;
	};
}