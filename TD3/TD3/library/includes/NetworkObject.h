#pragma once

#include "Compression.hpp"
#include "Decompression.hpp"

namespace uqac::replication {
	class NetworkObject {
	protected:
		int mClassID;

	public:
		enum { player, ennemy };
		inline NetworkObject() {
			mClassID = ennemy;
		};
		~NetworkObject() = default;

		virtual void Write(uqac::compression::Compression* compressor, bool verbose = false) = 0;
		virtual void Read(uqac::compression::Decompression* decompressor, bool verbose = false) = 0;
		virtual void Destroy(bool) = 0;

		inline virtual int getClassID() {
			return mClassID;
		};
		inline virtual float generateRandomFloat(float min, float max) {
			return min + static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / (max - min)));
		};

		inline virtual int generateRandomInt(int min, int max) {
			return min + rand() % (max - min);
		};

		virtual void randomModifications() = 0;
	};
}