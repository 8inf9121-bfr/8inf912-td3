#pragma once
#include <map>
#include <optional>

#include "../includes/NetworkObject.h"

namespace uqac::replication {
	class LinkingContext {
	private:
		std::map<int, std::shared_ptr<NetworkObject>> objectIdentifierToPointer;
		std::map<std::shared_ptr<NetworkObject>, int> pointerToObjectIdentifier;

	public:
		LinkingContext() = default;
		~LinkingContext() = default;

		/*
			Add NetworkObject to the current context and associate given id

			@param myObject is the NetworkObject to add to the context
			@param id will be associated to the NetworkObject

			@returns EXIT_SUCCES if it worked, EXIT_FAILURE if id is already taken
		*/
		int addObjectToContextWithId(std::shared_ptr<NetworkObject> myObject, int id);

		/*
			Remove NetworkObject from the current context

			@param myObject is the current object to delete

			@return EXIT_SUCCES if it worked, EXIT_FAILURE myObject is not in LinkingContext
		*/
		int removeObjectInContext(std::shared_ptr<NetworkObject> myObject);

		/*
			Add a NetworkObject in the context.
			Generate an id automaticly and return it.
			Try to generate a big negative int. Assumes users will choose positive id.  

			@param myObject is the NetworkObject to add to the context
		*/
		int addObjectToContext(std::shared_ptr<NetworkObject> myObject);

		/*
			Returns the identifier corresponding to myObject

			@param myObject is the NetworkObject whose identifier is sought
		*/
		std::optional<int> getIdentifier(std::shared_ptr<NetworkObject> myObject);

		/*
			Returns a pointer on the NetworkObject associated with the id

			@param myId is the identifier corresponding to the object sought
		*/
		std::optional<std::shared_ptr<NetworkObject>> getObjectPointer(int myId);
	};
}