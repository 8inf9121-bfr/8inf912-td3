#pragma once

#include "NetworkObject.h"
#include "LinkingContext.hpp"
#include "Serializer.h"
#include "Network.hpp"
#include "ClassRegistry.h"

#include <unordered_set>

namespace uqac::replication {
	class ReplicationManager {
	private:
		enum {
			HELLO,
			SYNC,
			BYE
		};

		std::unordered_set<std::shared_ptr<NetworkObject>> replicatedObjectSet;
		LinkingContext myLinkingContext;
		uqac::network::Network myNetworkManager;

	public:
		ReplicationManager();
		~ReplicationManager() = default;

		void sendUpdate();
		void receiveUpdate(uqac::compression::Decompression* decompressor);
		void addObject(std::shared_ptr<NetworkObject> newObject);
		void addObjectWithID(std::shared_ptr<NetworkObject> newObject, int objectID);
		void game(bool isServer);
		void connect();
	};
}