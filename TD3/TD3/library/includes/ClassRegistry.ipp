#pragma once

#include <functional>

#include "ClassRegistry.h"
#include "NetworkObject.h"

namespace uqac::replication {

	template <class T>
	void ClassRegistry::save (T object) {
		int classId = object.getClassID();
		std::function<std::shared_ptr<NetworkObject>()> test = []() { return std::make_shared<T>(); };

		ClassRegistry::getInstance().myMap.insert({ classId, test });

		return;
	}
}