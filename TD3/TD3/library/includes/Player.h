#pragma once

#include "NetworkObject.h"
#include "Framework.h"
#include "Compression.hpp"
#include "Decompression.hpp"

namespace uqac::replication {
	class Player : public NetworkObject {
	private:
		uqac::utils::Vector3<float> position;
		uqac::utils::Quaternion rotation;
		uqac::utils::Vector3<float> size;
		int life;
		int armor;
		float money;
		char name[128];

		std::pair<int, int> mClassIDRange;
		std::pair<float, float> positionXRange;
		std::pair<float, float> positionYRange;
		std::pair<float, float> positionZRange;
		std::pair<float, float> quaternionRange;
		std::pair<int, int> lifeRange;
		std::pair<int, int> armorRange;
		int defaultPrecision;
		std::pair<float, float> moneyRange;
		int moneyPrecision;

		void initRanges();

	public:
		Player();
		Player(uqac::utils::Vector3<float>, uqac::utils::Quaternion, uqac::utils::Vector3<float>, int, int, float, std::string);
		~Player() = default;

		virtual void Write(uqac::compression::Compression* compressor, bool verbose = false) override;
		virtual void Read(uqac::compression::Decompression* decompressor, bool verbose = false) override;
		virtual void Destroy(bool verbose) override;
		virtual void randomModifications() override;
	};
}