#include "Compression.hpp"
#include "Decompression.hpp"
#include "../includes/Ennemy.h"

namespace uqac::replication {
	Ennemy::Ennemy() : 
		position { uqac::utils::Vector3(0.f, 0.f, 0.f) },
		rotation { uqac::utils::Quaternion(1.f, 0.f, 0.f, 0.f) },
		type{ EnnemyType::sbire }, life{ 0 }
	{
		mClassID = ennemy;
		initRanges();
	}

	Ennemy::Ennemy(uqac::utils::Vector3<float> _position, uqac::utils::Quaternion _rotation, EnnemyType _type, int _life) :
		position { _position }, rotation { _rotation },
		type { _type }, life { _life }
	{
		mClassID = ennemy;
		initRanges();
	}

	void Ennemy::initRanges() {
		mClassIDRange = std::pair<int, int>(0, 255);
		mTypeRange = std::pair<int, int>(0, 255);
		positionXRange = std::pair<float, float>(-500.f, 500.f);
		positionYRange = std::pair<float, float>(-500.f, 500.f);
		positionZRange = std::pair<float, float>(0.f, 100.f);
		quaternionRange = std::pair<float, float>(-1.f, 1.f);
		lifeRange = std::pair<int, int>(0, 300);
		defaultPrecision = 3;
	}

	void Ennemy::Write(uqac::compression::Compression* compressor, bool verbose) {
		//compressor->compressIntWithBytePrecision(mClassID, mClassIDRange);
		//if (verbose)
		//	std:: cout << "[ENNEMY] Write() ClassId = " << mClassID << std::endl;
		compressor->compressIntWithBytePrecision(type, mTypeRange);
		if (verbose)
			std::cout << "[ENNEMY] Write() Type = " << type << std::endl;
		compressor->compressVector3Float(position, positionXRange, positionYRange, positionZRange, defaultPrecision);
		if (verbose)
			std::cout << "[ENNEMY] Write() Position = " << position << std::endl;
		compressor->compressQuaternion(rotation, defaultPrecision);
		if (verbose)
			std::cout << "[ENNEMY] Write() Rotation = " << rotation << std::endl;
		compressor->compressIntWithBytePrecision(life, lifeRange);
		if (verbose)
			std::cout << "[ENNEMY] Write() Life = " << life << std::endl;
	}

	void Ennemy::Read(uqac::compression::Decompression* decompressor, bool verbose) {
		//std::string buffer = compressor->getSerializerContent();
		//uqac::compression::Decompression decompressor(buffer.c_str(), buffer.size());

		//int compClassId = decompressor->decompressIntWithBytePrecision(mClassIDRange);
		//if (verbose)
		//	std::cout << "[ENNEMY] Read() ClassId = " << compClassId << std::endl;
		int compType = decompressor->decompressIntWithBytePrecision(mTypeRange);
		if (verbose)
			std::cout << "[ENNEMY] Read() Type = " << compType << std::endl;
		uqac::utils::Vector3<float> compPosition = decompressor->decompressVector3Float(positionXRange, positionYRange, positionZRange, defaultPrecision);
		if (verbose)
			std::cout << "[ENNEMY] Read() Position = " << compPosition << std::endl;
		uqac::utils::Quaternion compRotation = decompressor->decompressQuaternion(defaultPrecision);
		if (verbose)
			std::cout << "[ENNEMY] Read() Rotation = " << compRotation << std::endl;
		int compLife = decompressor->decompressIntWithBytePrecision(lifeRange);
		if (verbose)
			std::cout << "[ENNEMY] Read() Life = " << compLife << std::endl;
	}

	void Ennemy::Destroy(bool verbose) {
		// ???
	}

	void Ennemy::randomModifications() {
		this->position = uqac::utils::Vector3<float>(
			generateRandomFloat(positionXRange.first, positionXRange.second),
			generateRandomFloat(positionYRange.first, positionXRange.second),
			generateRandomFloat(positionZRange.first, positionXRange.second)
			);

		switch (generateRandomInt(0, 3))
		{
		case 0:
			this->rotation = uqac::utils::Quaternion(
				0.632f,
				0.548f,
				0.447f,
				0.316f
			);
			break;
		case 1:
			this->rotation = uqac::utils::Quaternion(
				0.f,
				1.f,
				0.f,
				0.f
			);
			break;
		case 2:
			this->rotation = uqac::utils::Quaternion(
				0.548f,
				0.316f,
				0.632f,
				0.447f
			);
			break;
		case 3:
			this->rotation = uqac::utils::Quaternion(
				0.f,
				0.f,
				1.f,
				0.f
			);
			break;
		default:
			break;
		}
		this->life = generateRandomInt(lifeRange.first, lifeRange.second);
	}
}