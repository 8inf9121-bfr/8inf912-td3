#include <iostream>

#include "../includes/ClassRegistry.h"

namespace uqac::replication {

	std::shared_ptr<NetworkObject> ClassRegistry::create(int id) {
		auto myMap = ClassRegistry::getInstance().myMap;
		auto search = myMap.find(id);

		if (search == myMap.end()) {
			std::cout << "[ClassRegistry] Unkown class" << std::endl;
		}

		std::function<std::shared_ptr<NetworkObject>()> myfunc = search->second;
		return myfunc();
	}
}