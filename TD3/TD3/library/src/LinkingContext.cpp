#include "../includes/LinkingContext.hpp"

namespace uqac::replication {

	int LinkingContext::addObjectToContextWithId(std::shared_ptr<NetworkObject> myObject, int id) {
		// Check if ID is not used yet
		auto isIdHere = this->objectIdentifierToPointer.find(id);
		if (isIdHere == this->objectIdentifierToPointer.end()) {
			this->objectIdentifierToPointer.insert(std::pair{ id, myObject });
			this->pointerToObjectIdentifier.insert(std::pair{ myObject, id });
			return EXIT_SUCCESS;
		}
		else {
			return EXIT_FAILURE;
		}

	}

	int LinkingContext::removeObjectInContext(std::shared_ptr<NetworkObject> myObject) {
		std::optional<int> idToErase = this->getIdentifier(myObject);
		if (idToErase.has_value()) {
			this->objectIdentifierToPointer.erase(idToErase.value());
			this->pointerToObjectIdentifier.erase(myObject);
			return EXIT_SUCCESS;
		}
		else {
			return EXIT_FAILURE;
		}
	}


	int LinkingContext::addObjectToContext(std::shared_ptr<NetworkObject> myObject) {
		int id = -2147483648;
		while (addObjectToContextWithId(myObject, id) == EXIT_FAILURE) {
			id++;
		}
		return id;
	}


	std::optional<int> LinkingContext::getIdentifier(std::shared_ptr<NetworkObject> myObject) {
		auto search = this->pointerToObjectIdentifier.find(myObject);
		if (search != this->pointerToObjectIdentifier.end()) {
			return search->second;
		}
		else {
			return {};
		}
	}

	std::optional<std::shared_ptr<NetworkObject>> LinkingContext::getObjectPointer(int myId) {
		auto search = this->objectIdentifierToPointer.find(myId);
		if (search != this->objectIdentifierToPointer.end()) {
			return search->second;
		}
		else {
			return {};
		}
	}
}



