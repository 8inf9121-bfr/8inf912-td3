#include <iostream>

#include "../includes/ReplicationManager.h"
#include "Compression.hpp"
#include "Decompression.hpp"
#include "../includes/Player.h"
#include "../includes/Ennemy.h"

static std::pair<int, int> rangeID(0, 255);
static std::pair<int, int> rangeObjectID(-2147483648, 2147483647);
static int currentPackageID = 0;
static int playersConnected = 0;

namespace uqac::replication {
	ReplicationManager::ReplicationManager() {
		this->myLinkingContext = LinkingContext();
		this->replicatedObjectSet = std::unordered_set<std::shared_ptr<NetworkObject>>();
		this->myNetworkManager = uqac::network::Network(STR_TCP_PROTOCOL);
	}

	void ReplicationManager::receiveUpdate(uqac::compression::Decompression* decompressor) {
		//uqac::compression::Decompression decompressor(buffer, buffersize);

		int size = decompressor->decompressIntWithBytePrecision(rangeID);
		int packageId = decompressor->decompressIntWithBytePrecision(rangeID);

		for (int i = 0; i < size; i++) {
			int objectID = decompressor->decompressIntWithBytePrecision(rangeObjectID);
			int classID = decompressor->decompressIntWithBytePrecision(rangeID);
			std::cout << "Id found is: " << objectID << std::endl;

			auto receivedObject = myLinkingContext.getObjectPointer(objectID);
			if (!receivedObject.has_value()) {
				std::cout << "[receiveUpdate] Object not found, we will now create it !" << std::endl; // --TODO--

				if (!uqac::replication::ClassRegistry::getInstance().classExists(classID)) {
					// add the class to the classRegistry if it doesn't exists yet
					std::shared_ptr newPlayer = std::make_shared<Player>();
					std::shared_ptr newEnnemy = std::make_shared<Ennemy>();

					switch (classID)
					{
					case uqac::replication::NetworkObject::player:
						uqac::replication::ClassRegistry::save<Player>(*newPlayer);
						addObjectWithID(newPlayer, objectID);
						newPlayer.get()->Read(decompressor, true);
						break;

					case uqac::replication::NetworkObject::ennemy:
						uqac::replication::ClassRegistry::save<Ennemy>(*newEnnemy);
						addObjectWithID(newEnnemy, objectID);
						newEnnemy.get()->Read(decompressor, true);
						break;

					default:
						break;
					}
				}
				else {
					auto newObject = uqac::replication::ClassRegistry::create(classID);
					addObjectWithID(newObject, objectID);
					newObject.get()->Read(decompressor, true);
				}
			}
			else {
				receivedObject.value().get()->Read(decompressor, true);
			}
		}
	}

	void ReplicationManager::sendUpdate() {
		uqac::compression::Compression compressor;
		// protocol id
		compressor.compressIntWithBytePrecision(SYNC, rangeID);

		// sends the number of elements
		compressor.compressIntWithBytePrecision(this->replicatedObjectSet.size(), rangeID);

		// package id -- TODO --
		compressor.compressIntWithBytePrecision(currentPackageID, rangeID);
		if (currentPackageID == 255) {
			currentPackageID = 0;
		}
		else {
			currentPackageID++;
		}

		for (const auto& elem : this->replicatedObjectSet) {
			//mySerializer.serialize<int>(SYNC);
			// 
			// object id
			if (!myLinkingContext.getIdentifier(elem)) {
				std::cout << "[Replication Manager] Object not found in LinkingContext" << std::endl;
				return;
			}

			int objectId = myLinkingContext.getIdentifier(elem).value();

			compressor.compressIntWithBytePrecision(objectId, rangeObjectID);

			// class id
			compressor.compressIntWithBytePrecision(elem.get()->getClassID(), rangeID);

			elem->Write(&compressor);
		}

		std::string bufferS = compressor.getSerializerContent();

		char* buffer = (char*)malloc(bufferS.size() * sizeof(char));
		if (buffer == NULL) {
			exit(1);
		}
		strcpy(buffer, bufferS.c_str());

		this->myNetworkManager.myConnection->broadcast(buffer, bufferS.size(), 0, [](char* buffer, int buffsize, int sock) {return; });

		return;
	}

	void  ReplicationManager::addObject(std::shared_ptr<NetworkObject> newObject) {
		if (myLinkingContext.getIdentifier(newObject).has_value()) {
			std::cout << "[ReplicationManager] Object already added" << std::endl;
			return;
		}

		myLinkingContext.addObjectToContext(newObject);
		this->replicatedObjectSet.insert(newObject);
	}

	void  ReplicationManager::addObjectWithID(std::shared_ptr<NetworkObject> newObject, int objectID) {
		if (myLinkingContext.getIdentifier(newObject).has_value()) {
			std::cout << "[ReplicationManager] Object already added" << std::endl;
			return;
		}

		myLinkingContext.addObjectToContextWithId(newObject, objectID);
		this->replicatedObjectSet.insert(newObject);
	}
	
	void ReplicationManager::game(bool isServer) {

		auto callbackServer = [this](char* buffer, int bufferSize, int sourceSocket) {
			uqac::compression::Decompression decompressorTemp(buffer, bufferSize);

			int protocol = decompressorTemp.decompressIntWithBytePrecision(std::pair<int, int>(0, 255));
			if (protocol == HELLO) {

				std::cout << "[ReplicationManager] A new player has arrived !" << std::endl;

				playersConnected++;

				//create a player
				std::shared_ptr newPlayer = std::make_shared<Player>();

				uqac::replication::ClassRegistry::save<Player>(*newPlayer);

				addObject(newPlayer);

				//eventually creates a new enemy
				if (playersConnected % 2 == 0) {
					std::shared_ptr newEnnemy = std::make_shared<Ennemy>();

					uqac::replication::ClassRegistry::save<Ennemy>(*newEnnemy);
					addObject(newEnnemy);
				}

				//send update to clients
				sendUpdate();

			}
			else if (protocol == SYNC) {
				//update reception
			}
		};

		auto callbackClient = [this](char* buffer, int bufferSize, int sourceSocket) {
			uqac::compression::Decompression decompressorTemp(buffer, bufferSize);
			// updates replication manager with received datas
			int protocol = decompressorTemp.decompressIntWithBytePrecision(std::pair<int, int>(0, 255));
			if (SYNC == protocol) {
				receiveUpdate(&decompressorTemp);
			}
		};

		if (isServer) {
			this->myNetworkManager.createServer(callbackServer);

			// randomly updates characters values
			for (;;){
				Sleep(5000);
				std::cout << "[Replication Manager] Update values" << std::endl;

				for (const auto& elem : this->replicatedObjectSet) {
					if (!myLinkingContext.getIdentifier(elem)) {
						std::cout << "[Replication Manager] Object not found in LinkingContext" << std::endl;
						return;
					}

					elem.get()->randomModifications();
				}
				sendUpdate();
			}
		}
		else {
			this->myNetworkManager.createClient(callbackClient);

			connect();
			for (;;) {}
		}
	}

	void ReplicationManager::connect() {
		uqac::compression::Compression compressor;
		compressor.compressIntWithBytePrecision(HELLO, rangeID);

		std::string bufferS = compressor.getSerializerContent();

		char* buffer = (char *)malloc(bufferS.size() * sizeof(char));
		if (buffer == NULL) {
			exit(1);
		}
		strcpy(buffer, bufferS.c_str());

		myNetworkManager.myConnection->sendMessage(buffer, bufferS.size(), 
			[](char* buffer, int size, int sizeSent) { std::cout << "[Player]: Connection complete" << std::endl; });
	}
}