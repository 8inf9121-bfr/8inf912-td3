#include "Compression.hpp"
#include "Decompression.hpp"
#include "../includes/Player.h"

namespace uqac::replication {
	Player::Player(uqac::utils::Vector3<float> _position, uqac::utils::Quaternion _rotation, 
		uqac::utils::Vector3<float> _size, int _life, int _armor, float _money, std::string _name) 
		:
		position { _position }, rotation { _rotation }, size { _size }, 
		life { _life }, armor { _armor }, money { _money }
	{
		mClassID = player;
		memset(name, 0, 128);
		memcpy(name, _name.c_str(), _name.length() > 127 ? 127 : _name.length());
		initRanges();
	}

	Player::Player() :
		position { uqac::utils::Vector3(0.f, 0.f, 0.f) },
		rotation { uqac::utils::Quaternion(1.f, 0.f, 0.f, 0.f) },
		size { uqac::utils::Vector3(0.f, 0.f, 0.f) },
		life { 0 }, armor { 0 }, money { 0 }
	{
		mClassID = player;
		memset(name, 0, 128);
		strcpy(name, "Name");
		initRanges();
	}

	void Player::initRanges() {
		mClassIDRange = std::pair<int, int>(0, 255);
		positionXRange = std::pair<float, float>(-500.f, 500.f);
		positionYRange = std::pair<float, float>(-500.f, 500.f);
		positionZRange = std::pair<float, float>(0.f, 100.f);
		quaternionRange = std::pair<float, float>(-1.f, 1.f);
		lifeRange = std::pair<int, int>(0, 300);
		armorRange = std::pair<int, int>(0, 50);
		defaultPrecision = 3;
		moneyRange = std::pair<float, float>(-99999.99f, 99999.99f);
		moneyPrecision = 2;
	}

	void Player::Write(uqac::compression::Compression* compressor, bool verbose) {
		//compressor->compressIntWithBytePrecision(mClassID, mClassIDRange);
		//if (verbose)
		//	std::cout << "[PLAYER] Write() ClassId = " << this->mClassID << std::endl;
		compressor->compressString(name);
		if (verbose)
			std::cout << "[PLAYER] Write() Name = " << name << std::endl;
		compressor->compressIntWithBytePrecision(life, lifeRange);
		if (verbose)
			std::cout << "[PLAYER] Write() Life = " << life << std::endl;
		compressor->compressIntWithBytePrecision(armor, armorRange);
		if (verbose)
			std::cout << "[PLAYER] Write() Armor = " << armor << std::endl;
		compressor->compressFloatWithBytePrecision(money, moneyRange, moneyPrecision);
		if (verbose)
			std::cout << "[PLAYER] Write() Money = " << money << std::endl;
		compressor->compressVector3Float(position, positionXRange, positionYRange, positionZRange, defaultPrecision);
		if (verbose)
			std::cout << "[PLAYER] Write() Position = " << position << std::endl;
		compressor->compressVector3Float(size, positionXRange, positionYRange, positionZRange, defaultPrecision);
		if (verbose)
			std::cout << "[PLAYER] Write() Size = " << size << std::endl;
		compressor->compressQuaternion(rotation, defaultPrecision);
		if (verbose)
			std::cout << "[PLAYER] Write() Rotation = " << rotation << std::endl;
	}

	void Player::Read(uqac::compression::Decompression* decompressor, bool verbose) {
		//std::string buffer = compressor->getSerializerContent();
		//uqac::compression::Decompression decompressor(buffer.c_str(), buffer.size());

		//int compClassId = decompressor->decompressIntWithBytePrecision(mClassIDRange);
		//if (verbose)
		//	std::cout << "[PLAYER] Read() ClassId = " << compClassId << std::endl;

		char compName[128] = {};
		decompressor->decompressString(compName);
		if (verbose)
			std::cout << "[PLAYER] Read() Name = " << compName << std::endl;
		int compLife = decompressor->decompressIntWithBytePrecision(lifeRange);
		if (verbose)
			std::cout << "[PLAYER] Read() Life = " << compLife << std::endl;
		int compArmor = decompressor->decompressIntWithBytePrecision(armorRange);
		if (verbose)
			std::cout << "[PLAYER] Read() Armor = " << compArmor << std::endl;
		float compMoney = decompressor->decompressFloatWithBytePrecision(moneyRange, moneyPrecision);
		if (verbose)
			std::cout << "[PLAYER] Read() Money = " << compMoney << std::endl;
		uqac::utils::Vector3<float> compPosition = decompressor->decompressVector3Float(positionXRange, positionYRange, positionZRange, defaultPrecision);
		if (verbose)
			std::cout << "[PLAYER] Read() Position = " << compPosition << std::endl;
		uqac::utils::Vector3<float> compSize = decompressor->decompressVector3Float(positionXRange, positionYRange, positionZRange, defaultPrecision);
		if (verbose)
			std::cout << "[PLAYER] Read() Size = " << compSize << std::endl;
		uqac::utils::Quaternion compRotation = decompressor->decompressQuaternion(defaultPrecision);
		if (verbose)
			std::cout << "[PLAYER] Read() Rotation = " << compRotation << std::endl;
	}

	void Player::Destroy(bool verbose) {
		// ???
	}

	void Player::randomModifications() {
		this->position = uqac::utils::Vector3<float>(
			generateRandomFloat(positionXRange.first, positionXRange.second),
			generateRandomFloat(positionYRange.first, positionXRange.second),
			generateRandomFloat(positionZRange.first, positionXRange.second)
		);

		switch (generateRandomInt(0,3))
		{
		case 0:
			this->rotation = uqac::utils::Quaternion(
				0.632f,
				0.548f,
				0.447f,
				0.316f
			);
			break;
		case 1:
			this->rotation = uqac::utils::Quaternion(
				0.f,
				1.f,
				0.f,
				0.f
			);
			break;
		case 2:
			this->rotation = uqac::utils::Quaternion(
				0.548f,
				0.316f,
				0.632f,
				0.447f
			);
			break;
		case 3:
			this->rotation = uqac::utils::Quaternion(
				0.f,
				0.f,
				1.f,
				0.f
			);
			break;
		default:
			break;
		}

		this->size = uqac::utils::Vector3<float> (
			generateRandomFloat(positionXRange.first, positionXRange.second),
			generateRandomFloat(positionYRange.first, positionXRange.second),
			generateRandomFloat(positionZRange.first, positionXRange.second)
		);
		this->life = generateRandomInt(lifeRange.first, lifeRange.second);
		this->armor = generateRandomInt(armorRange.first, armorRange.second);
		this->money = generateRandomFloat(moneyRange.first, moneyRange.second);
	}
}